﻿using Pools;
using System.Collections.Generic;
using UnityEngine;

namespace src
{
    public class Test : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        private TestBehaviour prefab;
#pragma warning restore 0649
        private ComponentPool<TestBehaviour> pool;
        private List<TestBehaviour> items = new List<TestBehaviour>();

        private void Awake()
        {
            pool = new ComponentPool<TestBehaviour>(prefab, 5, transform);
        }

        private void Start()
        {
            for (int i = 0; i < 12; i++)
            {
                var item = pool.Get();
                items.Add(item);
            }
        }
    }
}