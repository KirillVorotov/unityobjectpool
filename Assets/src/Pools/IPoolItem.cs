﻿namespace Pools
{
    public interface IPoolItem
    {
        void Init();
        void DeInit();
    }
}