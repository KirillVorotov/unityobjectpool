﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pools
{
    public class ComponentPool<TComponent> : IObjectPool<TComponent>
        where TComponent : MonoBehaviour, IPoolItem
    {
        private Stack<TComponent> inactiveItems;
        private TComponent prefab;
        private Transform parentTransform;
        private int activeCount = 0;

        public ComponentPool(TComponent prefab, Transform parentTransform = null)
        {
            inactiveItems = new Stack<TComponent>();
            this.prefab = prefab;
            this.parentTransform = parentTransform;
            if (prefab == null)
            {
                throw new ArgumentNullException("Prefab cannot be null.");
            }
        }

        public ComponentPool(TComponent prefab, int initialSize, Transform parentTransform = null)
        {
            if (initialSize < 0)
            {
                initialSize = 0;
            }
            inactiveItems = new Stack<TComponent>(initialSize);
            this.prefab = prefab;
            this.parentTransform = parentTransform;
            if (prefab == null)
            {
                throw new ArgumentNullException("Prefab cannot be null.");
            }
            for (int i = 0; i < initialSize; i++)
            {
                inactiveItems.Push(CreateNew());
            }
        }

        public int TotalCount
        {
            get
            {
                return inactiveItems.Count + activeCount;
            }
        }

        public int ActiveCount
        {
            get
            {
                return activeCount;
            }
        }

        public TComponent Get()
        {
            if (inactiveItems.Count == 0)
            {
                if (TotalCount == 0)
                {
                    ExpandBy(1);
                }
                else
                {
                    ExpandBy(TotalCount);
                }
            }

            var item = inactiveItems.Pop();
            activeCount++;
            item.Init();
            return item;
        }

        public void Return(TComponent item)
        {
            if (inactiveItems.Contains(item))
            {
                Debug.LogErrorFormat("Tried to return an item to pool {0} twice", GetType());
                return;
            }
            item.DeInit();
            activeCount--;
            inactiveItems.Push(item);
        }

        private TComponent CreateNew()
        {
            TComponent item = GameObject.Instantiate(prefab);
            item.transform.SetParent(parentTransform, true);
            item.DeInit();
            return item;
        }

        private void ExpandBy(int num)
        {
            int desiredCount = TotalCount + num;
            while(TotalCount < desiredCount)
            {
                inactiveItems.Push(CreateNew());
            }
        }
    }
}