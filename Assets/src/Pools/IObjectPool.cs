﻿namespace Pools
{
    public interface IObjectPool<T>
    {
        int TotalCount { get; }
        int ActiveCount { get; }
        T Get();
        void Return(T item);
    }
}