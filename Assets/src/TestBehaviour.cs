﻿using Pools;
using UnityEngine;

namespace src
{
    public class TestBehaviour : MonoBehaviour, IPoolItem
    {
        public void DeInit()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            gameObject.SetActive(false);
        }

        public void Init()
        {
            gameObject.SetActive(true);
            transform.SetParent(null);
            transform.localPosition = new Vector3(1, 1, 0);
        }
    }
}